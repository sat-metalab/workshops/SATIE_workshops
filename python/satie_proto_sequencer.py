#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created between Feb 15-27 2021

@author: Marc Lavallée <mlavallee@sat.qc.ca>


Prototype sequencer for SATIE.

It implements a SatieProtoSequencer class
to sequence positions and synthdef parameters
with a messaging queue to operate the sequencer.

This prototype is maint to run in a ipython3 terminal.
Use "mq.put()" to send messages ('play', 'pause', 'loop on', ...)
typical use: mq.put('loop on'), mq.put('play'), mq.put('pause')

the sequencer is immune to the ctrl-c signal (keyboard interruption)
so interactive commands can be sent while the sequencer is still running.
ex; when using plugins like 'zkarpluck1', trigger signals can be sent using:
satie_osc.send('/satie/source/set', 'zkarpluck1Ambi3_0', 't_trig', 1)
this command can be repeated from an infinite loop and stopped with ctrl-c

"""

# import sys
# import atexit
from multiprocessing import Process, Queue
import threading as th
import math
# import random
import time
import numpy as np
import itertools

from contextlib import suppress

import logging
logging.getLogger("matplotlib").setLevel(logging.WARNING)
logging.basicConfig(level=logging.DEBUG)


# Use in SCIDE to start a 3rd order Binaural renderer
SATIE_HOABinaural = """
(
    s = Server.supernova.local;
    // increase this if you need to load more samples :
    s.options.numBuffers = 1024 * 256 * 4;
    // increase this if you get "alloc failed" messages :
    s.options.memSize = 8192 * 32 * 4;
    // increase this if you are getting drop outs and the message "too many nodes" :
    s.options.maxNodes = 1024 * 32 * 4;

    // instantiate a SatieConfiguration.
    // Here we will use a HOA binaural spatializer
    ~satieConfiguration = SatieConfiguration.new(
        s,
        [],
        outBusIndex: [0],
        ambiOrders: [1, 2, 3],  // list ambisonic order handled by this satie instance
        minOutputBusChannels: 2
    );

    // choose which ambisonic encoder you wish to use
    // (\wave for plane or spherical wave encoding,
    //  \harmonic for better CPU performance)
    ~satieConfiguration.hoaEncoderType = \wave;

    // list possible listeners:
    ~satieConfiguration.spatializers.keys;
    ~satieConfiguration.server.options.numOutputBusChannels = 2;
    ~satieConfiguration.server.options.numInputBusChannels = 2;
    ~satieConfiguration.server.options.memSize = 2.pow(18).asInteger;
    ~satieConfiguration.server.options.numWireBufs = 2 * 16;
    // instantiate SATIE renderer and pass it the configuration
    ~satie = Satie.new(~satieConfiguration);

    ~satie.waitForBoot({
        // instantiate one ambisonic decoder for order 3;
        // set to 1 for less computation, 3 for more precision
        ~satie.replaceAmbiPostProcessor([\HOABinaural], spatializerNumber: 0, order: 3, outputIndex: ~satieConfiguration.outBusIndex);
        // display some information
        s.meter;
	    s.scope;
        s.plotTree;
    });
)
"""

# Use in SCIDE to start a cube VBAP renderer
SATIE_VBAPCUBE =""""
(
    s = Server.supernova.local;
    // increase this if you need to load more samples :
    s.options.numBuffers = 1024 * 256 * 4;
    // increase this if you get "alloc failed" messages :
    s.options.memSize = 8192 * 32 * 4;
    // increase this if you are getting drop outs and the message "too many nodes" :
    s.options.maxNodes = 1024 * 32 * 4;

    ~config = SatieConfiguration.new(s, [\cube], minOutputBusChannels: 8, outBusIndex: [0]);
    ~satie = Satie.new(~config);
    ~satie.waitForBoot({
    	// ~testSynth2 = ~satie.makeSourceInstance(\testSynth2,\default, \default);
    	// ~testSynth2.set(\gainDB, -20, \aziDeg, 0, \eleDeg, 0);
    	s.sync;
    	s.meter;
    	s.plotTree;
    });
)
"""

# Use in SCIDE for info on plugins
SATIE_INFOS = """
~satie.inspector.getPluginInfoJSON(\zkarpluck1);
~satie.synthDescLib.browse;
"""


def now():
    return time.time_ns() / (10 ** 9)


def xyz2sph(x, y, z):
    azimuth = math.degrees(math.atan2(y, x))
    xy2 = x ** 2 + y ** 2
    elevation = math.degrees(math.atan2(z, math.sqrt(xy2)))
    xyz2 = xy2 + z ** 2
    distance = np.clip(math.sqrt(xyz2), 0.1, 1000)
    return azimuth, elevation, distance


class SatieProtoSequencer(Process):
    def __init__(
        self,
        queue,
        satie,
        synthdefs,
        position_data,
        synthdef_data=np.empty(0),
        is_spherical=False,
        synth_params=[],
        distance_scaling=10,
        resolution=0.01,
        sources=[],
        *args,
        **kwargs,
    ):
        super().__init__()

        self.logger = logging.getLogger(self.__class__.__name__)
        self.queue = queue
        self.satie = satie
        self.synthdefs = synthdefs
        self.num_synthdefs = len(synthdefs)
        self.position_data = position_data
        self.synthdef_data = synthdef_data
        self.is_spherical = is_spherical
        self.synth_params = synth_params
        self.distance_scaling = distance_scaling
        self.resolution = resolution

        self.num_sources = self.position_data.shape[1]
        self.num_positions = self.position_data.shape[2]

        if sources:
            # consider only the provided synthdefs (as source numbers)
            self.sources = sources
        else:
            # consider all synthdefs
            self.sources = range(self.num_sources)

        self.playing = False
        self.looping = 0
        self.position = 0
        self.timers = []

        self.start_time = 0

    def __del__(self):
        self.logger.debug("delete")

    def seek(self, position=0):
        if position <= 0:
            self.position = 0
        elif position >= self.num_positions - 1:
            self.position = self.num_positions - 1
        else:
            self.position = position

    def peek(self):
        self.logger.info(self.position)

    def reset_timers(self):
        for t in self.timers:
            t.cancel()
        self.timers = []

    def play(self, playing=True, position=0):
        for synthdef in self.synthdefs:
            self.synthdef_state(synthdef, playing)

        if playing:
            self.position = position
            self.reset_timers()
            self.start_time = now()

        self.playing = playing
        self.logger.debug("set playing to %s at %s", self.playing, self.position)

    def set_position(self, msg=""):
        try:
            self.position = int(msg.split(" ")[1])
            if self.position < 0:
                self.position = 0
            self.logger.debug("set position to %s", self.position)
        except:
            self.logger.warning("invalid set_position message: %s", msg)
            pass

    def set_looping(self, msg=""):
        try:
            self.looping = int(msg.split(" ")[1])
            if self.looping < 0:
                self.looping = 0
            self.logger.debug("set looping to %s", self.looping)
        except:
            self.logger.warning("invalid set_looping message %s", msg)

    def set_resolution(self, msg=""):
        try:
            self.resolution = float(msg.split(" ")[1])
            self.logger.debug("set_resolution to %s", self.resolution)
        except:
            self.logger.warning("invalid set_resolution message: %s", msg)

    def set_update_from_msg(self, msg):
        try:
            synthdef, distance, azimuth, elevation = msg.split(" ")[1:]
            self.update_synthdef(
                synthdef, float(distance), float(azimuth), float(elevation)
            )
            self.logger.debug(
                "set_update_from_msg: %s, %s, %s %s",
                synthdef, distance, azimuth, elevation
            )
        except:
            self.logger.warning("invalid update message: %s", msg)

    def stop(self):
        self.reset_timers()
        if self.is_alive():
            self.terminate()
            self.join()
            self.close()

    def process_queue(self):
        if self.queue.empty():
            return
        msg = self.queue.get_nowait()
        if msg.startswith("update "):
            self.set_update_from_msg(msg)
        elif msg.startswith("resolution "):
            self.set_resolution(msg)
        if msg.startswith("position "):
            self.set_position(msg)
        if msg == ("position"):
            self.peek()
        elif msg == "play":
            self.play(True)
        elif msg == "pause":
            self.play(False)
        elif msg == "unpause":
            self.play(position=self.position + 1)
        elif msg.startswith("loop ") or msg.startswith("looping "):
            self.set_looping(msg)

    def run(self):
        self.logger.debug("start running")

        while True:
            self.process_queue()
            if not self.playing:
                time.sleep(0.01)
                continue

            with suppress(KeyboardInterrupt):

                if self.position < self.num_positions:
                    self.timers = []
                    self.start_time += self.resolution

                    for source in self.sources:

                        if self.position_data.any():
                            if self.is_spherical:
                                azimuth, elevation, distance = self.position_data[:,
                                                                                  source, self.position]
                            else:
                                # get point in spatial audio coordinate system
                                x, y, z = self.position_data[:, source, self.position]
                                azimuth, elevation, distance = xyz2sph(x, y, z)

                            distance *= self.distance_scaling

                            timer = th.Timer(
                                self.start_time - now(),
                                self.update_synthdef,
                                (self.synthdefs[source],
                                 distance, azimuth, elevation),
                            )

                            self.timers.append(timer)
                            timer.start()

                        if self.synthdef_data.any():

                            synth_data = list(itertools.chain(*zip(
                                self.synth_params, self.synthdef_data[:,
                                                                      source, self.position]
                                )))

                            # print(freq)
                            timer = th.Timer(
                                self.start_time - now(),
                                self.set_synthdef,
                                (self.synthdefs[source], synth_data)
                            )

                            self.timers.append(timer)
                            timer.start()

                    for t in self.timers:
                        t.join()

                    self.position += 1

                else:
                    if self.looping > 1:
                        self.logger.debug(
                            "looping %s more times", self.looping-1)
                        self.looping -= 1
                        self.position = 0
                    else:
                        self.play(False)
                        self.logger.debug("pause")

    def update_synthdef(self, synthdef, distance=0, azimuth=0, elevation=0, max_freq=0):
        # delay of sound transmission in ms, at standard speed of sound
        delay = 1000 * distance / 343

        # attenuation; 0 dB at 0.1 meter, -20dB per order or magnitude (in base 10)
        attenuation = 20 * (-1 + math.log10(1 / distance))

        # frequency attenuation (approximation)
        if max_freq:
            # set max_freq to :
            # 20000 for maximum freq range, no audible effects for distance < 150m
            # 16000 to get audible effects for distance > 50m
            lowpass = 100 + (max_freq - 100) * (1 - (distance / 1000)) ** 2
        else:
            lowpass = 15000

        self.satie.send(
            "/satie/source/update",
            synthdef,
            float(azimuth),
            float(elevation),
            attenuation,
            delay,
            lowpass,
        )


    def set_synthdef(self, synthdef, synth_data):
        self.satie.send(
            "/satie/source/set",
            synthdef,
            *synth_data
        )

    def synthdef_state(self, synthdef, state=False):
        state = 1 if state else 0
        self.satie.send("/satie/source/state", synthdef, state)

# end
